<?php

namespace Inmovsoftware\CommunityApi\Providers;

use Illuminate\Support\ServiceProvider;

use Inmovsoftware\CommunityApi\Http\Resources\V1\GlobalCollection;

class InmovTechCommunityServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('Inmovsoftware\CommunityApi\Models\V1\Community');
        $this->app->make('Inmovsoftware\CommunityApi\Http\Controllers\V1\CommunityController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }


}
